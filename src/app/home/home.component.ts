import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgModule } from '@angular/core';
import {FilmService} from '../film.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @NgModule({
    providers: [FilmService]
  })
  identForm!: FormGroup;
  isSubmited = false;
  tabfilm: any= [];
  

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      film: new FormControl(),
    });
  }
  film() {
    this.isSubmited = true;
    console.log('Données du formulaire...', this.identForm.value);
    this.filmService.rechercheFilm(this.identForm.value.film).subscribe((data: any)=> {
      this.filmService.addInfo(data['results']);
      this.tabfilm= this.filmService.getAllInfos();
      console.log(data['results'])
    })
  }
} 
