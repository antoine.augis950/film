import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { Film } from './film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {
  private infos: any=[];

  constructor(private http: HttpClient) { }

  public rechercheFilm(film: string){
    return this.http.get('https://api.themoviedb.org/3/search/movie?api_key='+environment.api+'&language=fr&query='+film) 

  }
  getAllInfos() {
    return this.infos
  }
  
  addInfo(Film: Film){
    this.infos = Film;
  }
}
